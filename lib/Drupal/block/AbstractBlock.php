<?php

namespace Drupal\block;

use Drupal\Config\DrupalConfig;
use Symfony\Component\Routing\RequestContext;

/**
 * @file
 *   An abstract block implementation to be inherited from to take care of many
 *   common block setup tasks.
 */
abstract class AbstractBlock implements BlockInterface {
  protected $config;
  protected $context;

  /**
   * Implements PluginInterface::__construct().
   */
  public function __construct(DrupalConfig $config = NULL) {
    $this->config = $config;
  }

  public function setContext(RequestContext $context) {
    $this->context = $context;
  }

  public function requiredContext() {
    return TRUE;
  }

  /**
   * Implements AccessInterface::access().
   */
  public function access() {
    return TRUE;
  }

  public function getBlock() {
    return $this->config;
  }

  /**
   * Implements BlockInterface::configure().
   */
  public function configure() {
    return array();
  }

  /**
   * Implements BlockInterface::configureValidate().
   */
  public function configureValidate($form, &$form_state) {}

  /**
   * Implements BlockInterface::configureSubmit().
   */
  public function configureSubmit($form, &$form_state) {}

  /**
   * Implements BlockInterface::build().
   */
  public function build() {}
}
