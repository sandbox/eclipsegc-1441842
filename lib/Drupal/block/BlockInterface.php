<?php

namespace Drupal\block;

use Drupal\Plugin\Access\AccessInterface;

/**
 * Interface definition for Block plugins.
 */
interface BlockInterface extends AccessInterface {

  /**
   * Retrieves information for the blocks admin pages.
   *
   * @return array().
   */
  public function info();

  /**
   * A getter for our protected config variable.
   *
   * @return $this->config.
   */
  public function getBlock();

  /**
   * The configuration form for the block.
   *
   * @return form array().
   */
  public function configure();

  /**
   * The validation for the configuration form.
   */
  public function configureValidate($form, &$form_state);

  /**
   * The submission for the configuration form.
   */
  public function configureSubmit($form, &$form_state);

  /**
   * A function for building renderable block arrays. A block theme wrapper
   * will be wrapped around this by the caller.
   *
   * @return renderable array().
   */
  public function build();
}
